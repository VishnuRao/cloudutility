package utility.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import utility.model.Note;
import utility.model.Password;
import utility.model.Reminder;
import utility.utility.ConnectionProvider;

public class PasswordDaoImpl {
	public int addPassword(Password pass) {
		String insertTableSQL = "INSERT INTO password"
				+ "(user, domain,password,hint) VALUES"
				+ "(?,?,?,?)";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setInt(1,pass.getUser());		
			ps.setString(2,pass.getDomain());	
			ps.setString(3,pass.getPassword());		
			ps.setString(4,pass.getHint());
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		return status;
	}
	public List<Password> listPassword() {
		List<Password> list=new ArrayList<Password>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement stmt = con.createStatement();
			ResultSet  rs = stmt.executeQuery("select * from password");
			while(rs.next()){
				list.add(new Password(rs.getInt("id"),rs.getInt("user"),rs.getString("domain"),rs.getString("password"),rs.getString("hint")));
			}
			return list;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
