package utility.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import utility.model.Reminder;
import utility.model.User;
import utility.utility.ConnectionProvider;

public class ReminderDaoImpl {
	public int addReminder(Reminder reminder) {
		String insertTableSQL = "INSERT INTO reminder"
				+ "(user, note,time) VALUES"
				+ "(?,?,?)";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setInt(1,reminder.getUser());		
			ps.setString(2,reminder.getNote());	
			ps.setString(3,reminder.getTime());		
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		return status;
	}
	public List<Reminder> listReminder(HttpSession session) {
		List<Reminder> list=new ArrayList<Reminder>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement stmt = con.createStatement();
			User u=(User) session.getAttribute("user");
			ResultSet rs=null;
			if(u!=null){
				 rs = stmt.executeQuery("select * from reminder where user="+u.getId()+" order by id");
			}else{
				 rs = stmt.executeQuery("select * from reminder");
			}
			while(rs.next()){
				list.add(new Reminder(rs.getInt("id"),rs.getInt("user"),rs.getString("note"),rs.getString("time")));
			}
			return list;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public int deleteReminder(int id) {
		String insertTableSQL ="DELETE from reminder WHERE id = ?";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setInt(1,id);		
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		return status;
	}
	public List<Reminder> listReminderDate(HttpSession session) {
		List<Reminder> list=listReminder(session);
		List<Reminder> nl=new ArrayList<Reminder>();
		for(Reminder t:list){
			Timestamp ts=Timestamp.valueOf(t.getTime());
			Timestamp nts=new Timestamp(System.currentTimeMillis());
			if(ts.before(nts)){
				System.out.println("--@"+t.getId());
				nl.add(t);
				deleteReminder(t.getId());
			}
		}
		return nl;
	}
}
