package utility.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import utility.model.Note;
import utility.model.User;
import utility.utility.ConnectionProvider;

public class NoteDaoImpl {
	public int addNote(utility.model.Note note) {
		String insertTableSQL = "INSERT INTO note"
				+ "(user, note,time) VALUES"
				+ "(?,?,?)";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setInt(1,note.getUser());		
			ps.setString(2,note.getNote());	
			ps.setString(3,note.getTime());		
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		return status;
	}
	public List<Note> listNote(HttpSession session) {
		List<Note> NoteList=new ArrayList<Note>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement stmt = con.createStatement();
			User u=(User) session.getAttribute("user");
			ResultSet  rs=null;
			if(u!=null){
				rs = stmt.executeQuery("select * from note where user="+u.getId()+" order by id");
			}else{
				rs = stmt.executeQuery("select * from note order by id");
			}
			while(rs.next()){
				NoteList.add(new Note(rs.getInt("id"),rs.getInt("user"),rs.getString("note"),rs.getString("time")));
			}
			return NoteList;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
