package utility.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import utility.model.Bookmark;
import utility.utility.ConnectionProvider;

public class BookmarkDaoImpl {
	public int addBookmark(Bookmark Bookmark) {
		String insertTableSQL = "INSERT INTO Bookmark"
				+ "(user, title,url) VALUES"
				+ "(?,?,?)";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setInt(1,Bookmark.getId());		
			ps.setString(2,Bookmark.getTitle());	
			ps.setString(3,Bookmark.getUrl());		
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		System.out.println("@@ val:  "+status);
		return status;
	}
	public int deleteBookmark(String Bookmarkname) {
		String insertTableSQL ="DELETE from Bookmark WHERE title = ?";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setString(1,Bookmarkname);		
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		return status;
	}
	public Bookmark getGetBookmarkByBookmarkname(String Bookmarkname) {
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement("select * from Bookmark where title=?");
			ps.setString(1,Bookmarkname);			
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				return new Bookmark(rs.getInt("id"),rs.getInt("user"),rs.getString("title"),rs.getString("url"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public Bookmark getById(int Bookmarkname) {
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement("select * from Bookmarks where id=?");
			ps.setInt(1,Bookmarkname);			
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				return new Bookmark(rs.getInt("id"),rs.getInt("user"),rs.getString("title"),rs.getString("url"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public List<Bookmark> listBookmark() {
		List<Bookmark> BookmarkList=new ArrayList<Bookmark>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement stmt = con.createStatement();
			ResultSet  rs = stmt.executeQuery("select * from Bookmark");
			while(rs.next()){
				BookmarkList.add(new Bookmark(rs.getInt("id"),rs.getInt("user"),rs.getString("title"),rs.getString("url")));
			}
			return BookmarkList;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public List<Bookmark> searchWithName(String name) {
		List<Bookmark> BookmarkList=new ArrayList<Bookmark>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement stmt = con.createStatement();
			ResultSet  rs = stmt.executeQuery("select * from Bookmarks where name like '%"+name+"%'");
			while(rs.next()){
				BookmarkList.add(new Bookmark(rs.getInt("id"),rs.getInt("user"),rs.getString("title"),rs.getString("url")));
			}
			return BookmarkList;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
