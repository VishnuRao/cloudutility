package utility.Dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import utility.model.User;
import utility.utility.ConnectionProvider;

public class UserDaoImpl {
	public int addUser(User user) {
		String insertTableSQL = "INSERT INTO user"
				+ "(USERNAME, PASSWORD,NAME) VALUES"
				+ "(?,?,?)";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setString(1,user.getUsername());		
			ps.setString(2,user.getPassword());	
			ps.setString(3,user.getName());	
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		System.out.println("@@ val:  "+status);
		return status;
	}
	public int deleteUser(String username) {
		String insertTableSQL ="DELETE from user WHERE username = ?";
		int status=-1;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement(insertTableSQL);
			ps.setString(1,username);		
			status=ps.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}	
		return status;
	}
	public User getGetUserByUsername(String username) {
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement("select * from user where username=?");
			ps.setString(1,username);			
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				return new User(rs.getInt("id"),rs.getString("username"),rs.getString("password"),rs.getString("name"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public User getById(int username) {
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement("select * from user where id=?");
			ps.setInt(1,username);			
			ResultSet rs=ps.executeQuery();
			if(rs.next()){
				return new User(rs.getInt("id"),rs.getString("username"),rs.getString("password"),rs.getString("name"));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public List<User> listUsers() {
		List<User> userList=new ArrayList<User>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement stmt = con.createStatement();
			ResultSet  rs = stmt.executeQuery("select * from user");
			while(rs.next()){
				userList.add(new User(rs.getInt("id"),rs.getString("username"),rs.getString("password"),rs.getString("name")));
			}
			return userList;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public List<User> searchWithName(String name) {
		List<User> userList=new ArrayList<User>();
		try{
			Connection con=ConnectionProvider.getCon();
			Statement stmt = con.createStatement();
			ResultSet  rs = stmt.executeQuery("select * from user where name like '%"+name+"%'");
			while(rs.next()){
				userList.add(new User(rs.getInt("id"),rs.getString("username"),rs.getString("password"),rs.getString("name")));
			}
			return userList;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public static boolean validate(User bean){
		boolean status=false;
		try{
			Connection con=ConnectionProvider.getCon();
			PreparedStatement ps=con.prepareStatement("select * from user where username=? and password=?");
			ps.setString(1,bean.getUsername());
			ps.setString(2, bean.getPassword());
			ResultSet rs=ps.executeQuery();
			status=rs.next();
		}catch(Exception e){
			e.printStackTrace();
		}
		return status;
	}
	public boolean changePassword(String email, String password) {
		 try {
		 Connection conn = ConnectionProvider.getCon();
	      String query = "update user set password = ? where username= ?";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString(1, password);
	      preparedStmt.setString(2, email);
	      preparedStmt.executeUpdate();
		 } catch (SQLException e) {
			 e.printStackTrace();
				return false;
		}
	      return true;
	}

}
