package utility.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import utility.model.User;
public class Filter implements javax.servlet.Filter {
	 public void doFilter(ServletRequest req, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		 List <String> list=new ArrayList<String>();
		    list.add("register");
		    list.add("login");
		    list.add("logout");
		    list.add("index");
		    list.add("home");
		    list.add("change");
		    list.add("addNote");
		    list.add("addReminder");
		    list.add("addBookmark");
		    list.add("addPassword");
		    list.add("listNote");
		    list.add("listReminder");
		    list.add("listBookmark");
		    list.add("listPassword");
		    
		    
	        HttpServletRequest request = (HttpServletRequest) req;
	        String path = ((HttpServletRequest) req).getRequestURI().substring(((HttpServletRequest) req).getContextPath().length());
	        path=path.replaceFirst("/", "");
	        if(path.matches(".*(css|jpg|png|gif|js)") || list.contains(path))
		    	chain.doFilter(req, response);
	        else if(path=="" || path.equals("")){
	        	HttpSession session = request.getSession(true);
	    		if (session.getAttribute("user") != null) { 
	    			User u=(User) session.getAttribute("user");
	    				req.getRequestDispatcher("/WEB-INF/home.jsp").forward(request,response);	    			
	    			  return;
	    		}else{
	    			req.getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
	    			  return;
	    		}
	        }else{
				req.getRequestDispatcher("index").forward(request, response);
			}
	    }
	    public void init(FilterConfig config) throws ServletException {
	        System.out.println("Filter init ");
	    }
	    public void destroy() {
	    }
}
