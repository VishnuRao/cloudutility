package utility.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.Dao.BookmarkDaoImpl;
import utility.model.Bookmark;
import utility.model.User;
@WebServlet("/addBookmark")
public class addBookmark extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addBookmark.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String title=request.getParameter("title");
		String url=request.getParameter("url");
		User u=(User) session.getAttribute("user");
		Bookmark ob= new Bookmark(u.getId(), title, url);
		
		BookmarkDaoImpl daoImpl = new BookmarkDaoImpl();
		if(daoImpl.addBookmark(ob)>0){
			request.getSession().setAttribute("success","Added successfully");
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addBookmark.jsp").forward(request,response);
		}else{
			request.getSession().setAttribute("fail","Unable to Add");
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addBookmark.jsp").forward(request,response);
		}
	}

}
