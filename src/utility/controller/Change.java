package utility.controller;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utility.Dao.UserDaoImpl;
import utility.model.User;

/**
 * Servlet implementation class Change
 */
@WebServlet("/change")
public class Change extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public Change() {
		super();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/changePassword.jsp").forward(request,response);
		
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String currentPassword = request.getParameter("currentPassword");
		String newPassword = request.getParameter("newPassword");
		User user = (User) request.getSession().getAttribute("user");
		UserDaoImpl dao=new UserDaoImpl();
		if (currentPassword.equals(user.getPassword())) {
			if (dao.changePassword(user.getUsername(), newPassword)) {
				request.getSession().setAttribute("password changed", "Password Changed Sucessfully");
			} else {
				request.getSession().setAttribute("password not changed", "Please try again!!!");
			}
		} else {
			request.getSession().setAttribute("password not changed", "Incorrect password");
		}
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/changePassword.jsp").forward(request,response);
	}
}