package utility.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.Dao.BookmarkDaoImpl;
import utility.model.Bookmark;

@WebServlet("/listBookmark")
public class listBookmark extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		BookmarkDaoImpl dao=new BookmarkDaoImpl();
		List<Bookmark> list=dao.listBookmark();
		 if(session!=null)  
		     session.setAttribute("listOfObjects", list); 
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/listBookmark.jsp").forward(request,response);
	}

}
