package utility.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.Dao.PasswordDaoImpl;
import utility.model.Password;
import utility.model.User;
@WebServlet("/addPassword")
public class addPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addPassword.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String domain=request.getParameter("domain");
		String password=request.getParameter("password");
		String hint=request.getParameter("hint");
		User u=(User) session.getAttribute("user");
		Password ob= new Password(u.getId(), domain, password,hint);
		
		PasswordDaoImpl daoImpl = new PasswordDaoImpl();
		if(daoImpl.addPassword(ob)>0){
			request.getSession().setAttribute("success","Added successfully");
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addPassword.jsp").forward(request,response);
		}else{
			request.getSession().setAttribute("fail","Unable to Add");
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addPassword.jsp").forward(request,response);
		}
	}

}
