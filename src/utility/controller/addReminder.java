package utility.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.Dao.ReminderDaoImpl;
import utility.model.Reminder;
import utility.model.User;
@WebServlet("/addReminder")
public class addReminder extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addReminder.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String note=request.getParameter("note");
		String date=request.getParameter("date");
		String time=request.getParameter("time");
		System.out.println("date :  "+date+"--time:  "+time);
		
		User u=(User) session.getAttribute("user");
		Reminder ob= new Reminder(u.getId(), note, date+" "+time+"00:00");
		
		ReminderDaoImpl daoImpl = new ReminderDaoImpl();
		if(daoImpl.addReminder(ob)>0){
			request.getSession().setAttribute("success","Added successfully");
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addReminder.jsp").forward(request,response);
		}else{
			request.getSession().setAttribute("fail","Unable to Add");
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addReminder.jsp").forward(request,response);
		}
	}

}
