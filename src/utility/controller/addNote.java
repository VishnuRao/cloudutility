package utility.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.Dao.NoteDaoImpl;
import utility.model.Note;
import utility.model.User;
@WebServlet("/addNote")
public class addNote extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addNote.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		String note=request.getParameter("note");
		
			User u=(User) session.getAttribute("user");
		Note ob= new Note(u.getId(), note, new Date().toString());
		
		NoteDaoImpl daoImpl = new NoteDaoImpl();
		if(daoImpl.addNote(ob)>0){
			request.getSession().setAttribute("success","Added successfully");
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addNote.jsp").forward(request,response);
		}else{
			request.getSession().setAttribute("fail","Unable to Add");
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/addNote.jsp").forward(request,response);
		}
	}

}
