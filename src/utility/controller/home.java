package utility.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.Dao.ReminderDaoImpl;
import utility.model.Reminder;
@WebServlet("/home")
public class home extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public home() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		ReminderDaoImpl dao=new ReminderDaoImpl();
		if (session!=null&&session.getAttribute("user") != null) { 
				List<Reminder> list=dao.listReminderDate(session);
			 	session.setAttribute("listOfObjects", list); 
			   getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request,response);
			  return;
		}	  
	}
}
