package utility.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utility.Dao.UserDaoImpl;
import utility.model.User;

@WebServlet("/register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public Register() {
        super();
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/registration.jsp").forward(request,response);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();    
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		String email=request.getParameter("username");
		UserDaoImpl p=new UserDaoImpl();
		List<User> list=p.searchWithName(name) ;
		User obj=new User(email,password,name);
		obj.setName(name);
		if(list.size()!=0){
			request.getSession().setAttribute("fail","Unable to Register");
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/registration.jsp");
				rd.include(request, response);
		}
		if(p.addUser(obj)>=1){
			request.getSession().setAttribute("success","Registration Done successfully");
				RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/registration.jsp");
				rd.include(request, response);
			
		}else{
				out.print("<p style=\"color:red\">Please enter proper details</p>");
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/registration.jsp").forward(request,response);
		}
	}

}
