package utility.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.Dao.ReminderDaoImpl;
import utility.model.Reminder;

@WebServlet("/listReminder")
public class listReminder extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		ReminderDaoImpl dao=new ReminderDaoImpl();
		List<Reminder> list=dao.listReminder(session);
		 if(session!=null)  
		     session.setAttribute("listOfObjects", list); 
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/listReminder.jsp").forward(request,response);
	}

}
