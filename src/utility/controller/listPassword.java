package utility.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.Dao.PasswordDaoImpl;
import utility.model.Password;

@WebServlet("/listPassword")
public class listPassword extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		PasswordDaoImpl dao=new PasswordDaoImpl();
		List<Password> list=dao.listPassword();
		 if(session!=null)  
		     session.setAttribute("listOfObjects", list); 
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/listPassword.jsp").forward(request,response);
	}

}
