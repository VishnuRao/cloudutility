package utility.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.Dao.ReminderDaoImpl;
import utility.Dao.UserDaoImpl;
import utility.model.Reminder;
import utility.model.User;

@WebServlet("/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Login() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		if (session.getAttribute("user") != null) { 
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request,response);
			  return;
		}else{
			getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request,response);
			  return;
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		if (session.getAttribute("user") != null) { 
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request,response);
			  return;
		}
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		UserDaoImpl daoImpl = new UserDaoImpl();
		 PrintWriter out = response.getWriter();    

		User dbUser = daoImpl.getGetUserByUsername(username);
		if (dbUser != null && dbUser.getPassword().equals(password)) {
			 if(session!=null)  
			     session.setAttribute("user", dbUser); 
			 
			  	ReminderDaoImpl dao=new ReminderDaoImpl();
				List<Reminder> list=dao.listReminderDate(session);
				session.setAttribute("listOfObjects", list); 
				getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/home.jsp").forward(request,response);
				return;
		} else {
			out.print("<p style=\"color:red\">Sorry username or password error</p>");
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/index.jsp");
			rd.include(request, response);
		}

		response.getWriter();
	}

}
