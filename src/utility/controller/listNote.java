package utility.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import utility.Dao.NoteDaoImpl;
import utility.model.Note;

@WebServlet("/listNote")
public class listNote extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		NoteDaoImpl dao=new NoteDaoImpl();
		List<Note> list=dao.listNote(session);
		 if(session!=null)  
		     session.setAttribute("listOfObjects", list); 
		getServletConfig().getServletContext().getRequestDispatcher("/WEB-INF/listNote.jsp").forward(request,response);
	}

}
