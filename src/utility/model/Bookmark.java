package utility.model;

public class Bookmark {
	private int id;
	private int user;
	private String title;
	private String url;
	
	public Bookmark(int user, String title, String url) {
		super();
		this.user = user;
		this.title = title;
		this.url = url;
	}
	
	public Bookmark(int id, int user, String title, String url) {
		super();
		this.id = id;
		this.user = user;
		this.title = title;
		this.url = url;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	
}
