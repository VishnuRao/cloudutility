package utility.model;

public class Reminder {
	private int id;
	private int user;
	private String note;
	private String time;
	public int getId() {
		return id;
	}
	
	public Reminder(int user, String note, String time) {
		super();
		this.user = user;
		this.note = note;
		this.time = time;
	}

	public Reminder(int id, int user, String note, String time) {
		super();
		this.id = id;
		this.user = user;
		this.note = note;
		this.time = time;
	}

	public void setId(int id) {
		this.id = id;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
}
