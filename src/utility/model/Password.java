package utility.model;

public class Password {
	private int id;
	private int user;
	private String domain;
	private String password;
	private String hint;
	public int getId() {
		return id;
	}
	
	public Password(int user, String domain, String password, String hint) {
		super();
		this.user = user;
		this.domain = domain;
		this.password = password;
		this.hint = hint;
	}

	public Password(int id, int user, String domain, String password, String hint) {
		super();
		this.id = id;
		this.user = user;
		this.domain = domain;
		this.password = password;
		this.hint = hint;
	}

	public void setId(int id) {
		this.id = id;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getHint() {
		return hint;
	}
	public void setHint(String hint) {
		this.hint = hint;
	}
	
}
