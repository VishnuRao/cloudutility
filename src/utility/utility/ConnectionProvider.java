package utility.utility;
import static utility.utility.Provider.*;

import java.sql.Connection;
import java.sql.DriverManager;
public class ConnectionProvider {
	static Connection con=null;
	static{
		try{
			Class.forName(DRIVER);
			con=DriverManager.getConnection(CONNECTION_URL,USERNAME,PASSWORD);
			}catch(Exception e){
				e.printStackTrace();
			}
	}
	public static Connection getCon(){
		return con;
	}
}
