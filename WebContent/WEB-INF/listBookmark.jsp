 <%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
 <%@ page import="java.util.*,utility.model.User,utility.model.Bookmark"%> 
 <html>
	<head>
		<%@include file="com/header.jsp"%>
		<style type="text/css">
			.content{
				width:50%;
			}
		</style>
	</head>
	<body >	
		<%@include file="com/nav1.jsp"%>
		<div id="content">
		<div class="content">
		<h1>Bookmarks</h1>
			<table border=1 style="margin:auto">
					<thead>
						<tr><th>Title</th><th>URL</th></tr>
					</thead>
					<%
						List<Bookmark> list=(ArrayList)session.getAttribute("listOfObjects");
						for(Bookmark t:list){
					%>
					<tr>
					<td ><%=t.getTitle()%></td>
					<td ><%=t.getUrl()%></td>
					</tr>
					<%} %>
				</table>
		</div>
		</div>
	</body>
</html>                                                                                                                                                                                                                                                  