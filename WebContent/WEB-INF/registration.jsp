<html>
	<head>
		<%@include file="com/header.jsp"%>
		<script type="text/javascript">
			function validate(){
						p=document.getElementById("password").value;
						n=document.getElementById("name").value;
						un=document.getElementById("username").value;
						cp=document.getElementById("cpassword").value;
						
						if(p!=cp){
							document.getElementById("errorMessage").innerHTML = "Passwords mismatch";
							return false;
						}
						if(p.length < 6) {
							document.getElementById("errorMessage").innerHTML = "Password length should be atleast 6";
							return false;
						}
						if(un.length < 2 || n.length < 2 ) {
							document.getElementById("errorMessage").innerHTML = "length should be atleast 1";
							return false;
						}
				return true;
			}
		</script>
	</head>
	<body >	
		<%@include file="com/nav.jsp"%>
		<div id="content">
		<div class="content">
		<div style="width:30%;margin:auto">
		<h1>Registration</h1>
			<form action="register" method="post" onsubmit="return validate()">
				<input type="text" id="name" name="name" placeholder="Name">
				<input type="text" id="username" name="username" placeholder="Username">				
				<input type="password" id="password" name="password" placeholder="password">
				<input type="password" id="cpassword" name="cpassword" placeholder="Confirm password">
					<span id="errorMessage" class="errorMsg" style="color:red"></span>
					<span class="successMsg" style="color:#2DFD2D"> <%if(session.getAttribute("success")!=null){out.print(session.getAttribute("success")); session.setAttribute("success",null);}%></span>
					<span class="errorMsg" style="color:red"> <%if(session.getAttribute("fail")!=null){out.print(session.getAttribute("fail"));session.setAttribute("fail",null);} %></span>
					<br>
				<input type="submit" value="Register" >

			</form>
		</div>
		</div>
		</div>
		
	</body>
</html>