<html>
	<head>
		<%@include file="com/header.jsp"%>
		<script type="text/javascript">
			function validate(){
						d=document.getElementById("domain").value;
						p=document.getElementById("password").value;
						h=document.getElementById("hint").value;
						if(p.trim().length < 1 || d.trim().length < 1 || h.trim().length < 1) {
							document.getElementById("errorMessage").innerHTML = "Enter Proper details";
							return false;
						}
				return true;
			}
		</script>
	</head>
	<body >	
		<%@include file="com/nav1.jsp"%>
		<div id="content">
		<div class="content">
		<div style="width:30%;margin:auto">
		<h1>Password</h1>
			<form action="addPassword"  method="post" onsubmit="return validate()">
				<input type="text" id="domain" name="domain" placeholder="Domain"><br>				
				<input type="password" id="password" name="password" placeholder="Password"><br>
				<input type="text" name="hint" id="hint" placeholder="Hint">
				<br>
				<input type="submit" value="Add Password">
			</form>
						<br>
			<span id="errorMessage" class="errorMsg" style="color:red"></span>
			<span class="successMsg" style="color:#2DFD2D"> <%if(session.getAttribute("success")!=null){out.print(session.getAttribute("success")); session.setAttribute("success",null);}%></span>
			<span class="errorMsg" style="color:red"> <%if(session.getAttribute("fail")!=null){out.print(session.getAttribute("fail"));session.setAttribute("fail",null);} %></span>
		</div>
		</div>
		</div>
	</body>
</html>