  <%@ page import="java.util.*,utility.model.User"%>
<%User user=(User)session.getAttribute("user"); %>
<ul id="nav">
			<li><a href="home">Home</a></li>
			<li><a href="addNote">Add Note</a></li>
			<li><a href="addReminder">Add Reminder</a></li>
			<li><a href="addPassword">Add Password</a></li>
			<li><a href="addBookmark">Add Bookmark</a></li>
			<li><a href="listNote">Note List </a></li>
			<li><a href="listReminder">All Reminders </a></li>
			<li><a href="listPassword">Password List </a></li>
			<li><a href="listBookmark">Bookmark List </a></li>
			<%if(user!=null){ %>
	                    <li>
	                        <a class="page-scroll" href="logout">logout</a>
	                    </li>
                      <%} %>
		</ul>