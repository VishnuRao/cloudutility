<html>
	<head>
		<%@include file="com/header.jsp"%>
		<script type="text/javascript">
			function validate(){
						t=document.getElementById("note").value;
						if(t.length < 1) {
							document.getElementById("errorMessage").innerHTML = "Enter Proper details";
							return false;
						}
				return true;
			}
		</script>
	</head>
	<body >	
		<%@include file="com/nav1.jsp"%>
		<div id="content">
		<div class="content">
		<div style="width:30%;margin:auto">
		<h1>Reminder</h1>
			<form action="addReminder"  method="post" onsubmit="return validate()">
				<input type="text" id="note" name="note" placeholder="Reminder Note">	
				<input type="date" id="date" name="date" placeholder="Date">
				<input type="time" id="time" name="time" placeholder="time">			
				<br>
				<input type="submit" value="Add Reminder">
			</form>
			<br>
			<span id="errorMessage" class="errorMsg" style="color:red"></span>
			<span class="successMsg" style="color:#2DFD2D"> <%if(session.getAttribute("success")!=null){out.print(session.getAttribute("success")); session.setAttribute("success",null);}%></span>
			<span class="errorMsg" style="color:red"> <%if(session.getAttribute("fail")!=null){out.print(session.getAttribute("fail"));session.setAttribute("fail",null);} %></span>
		</div>
		</div>
		</div>
	</body>
</html>